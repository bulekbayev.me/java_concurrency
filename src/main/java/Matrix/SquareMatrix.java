package Matrix;

import Resources.ApplicationData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class SquareMatrix {

    private int matrixSize;
    private int[][] matrix;
    private final Logger logger;

    public SquareMatrix(int matrixSize) {
        this.matrixSize = matrixSize;
        logger = LogManager.getLogger();
    }

    public void initializeMatrix() {
        matrix = new int[matrixSize][matrixSize];
        insertRandomIntegerIntoMatrix(matrix);
    }

    public int getMatrixElementAt(int indexOfElement) {
        return matrix[indexOfElement][indexOfElement];
    }

    public void setMatrixElementAt(int indexOfElement, int elementNewValue) {
        matrix[indexOfElement][indexOfElement] = elementNewValue;
    }

    public int getMatrixSize() {
        return matrix.length;
    }

    public void changeElementOfMatrixMainDiagonal(ReentrantLock matrixElementLock, int threadUniqueInteger, int indexOfTheElementInMainDiagonal) {
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            logger.error(Thread.currentThread().getName() + " was interrupted.");;
        }
        if (matrixElementLock.tryLock()) {
            try {
                if (getMatrixElementAt(indexOfTheElementInMainDiagonal) == 0) {
                    setMatrixElementAt(indexOfTheElementInMainDiagonal, threadUniqueInteger);
                    logger.info(Thread.currentThread().getName() + " value: " + getMatrixElementAt(indexOfTheElementInMainDiagonal) + " index: " + indexOfTheElementInMainDiagonal);
                }
            } finally {
                matrixElementLock.unlock();
            }
        }
    }

    private void insertRandomIntegerIntoMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <matrix[i].length; j++) {
                if (i == j) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = getRandomNumberInRange(new ApplicationData().getPropertyValue("MATRIX_MIN_RANGE_INTEGER"),
                                                          new ApplicationData().getPropertyValue("MATRIX_MAX_RANGE_INTEGER"));
                }
            }
        }
    }

    private int getRandomNumberInRange(int minimumRandomIntegerValue, int maximumRandomIntegerValue) {
        if (minimumRandomIntegerValue >= maximumRandomIntegerValue) {
            logger.error("Maximum must be greater than minimum!");
        }
        Random randomizer = new Random();
        return randomizer.nextInt((maximumRandomIntegerValue - minimumRandomIntegerValue) + 1) + minimumRandomIntegerValue;
    }

    public void printMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
