package Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationData {

    public int getPropertyValue(String propertyName) {
        String propertyValue = "";
        Properties properties = new Properties();
        try (InputStream inputStream = this.getClass().getResourceAsStream("/application.properties")) {
            properties.load(inputStream);
            propertyValue = properties.getProperty(propertyName);
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        return Integer.parseInt(propertyValue);
    }
}
