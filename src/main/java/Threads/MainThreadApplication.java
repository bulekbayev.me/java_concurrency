package Threads;

import Matrix.SquareMatrix;
import Resources.ApplicationData;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class MainThreadApplication {

    public static void main(String[] args) throws InterruptedException {
        ApplicationData applicationData = new ApplicationData();
        ArrayList<Thread> arrayListOfThreads = new ArrayList();
        ReentrantLock diagonalElementLock = new ReentrantLock();

        int threadsCount = applicationData.getPropertyValue("THREADS_QUANTITY");
        SquareMatrix matrix = new SquareMatrix(applicationData.getPropertyValue("MATRIX_SIZE"));
        matrix.initializeMatrix();

        for (int i = 0; i < threadsCount; i++) {
            Thread myThread = new MatrixDiagonalThread(matrix, applicationData.getPropertyValue("THREAD_" + i + "_UNIQUE_INTEGER"), diagonalElementLock);
            arrayListOfThreads.add(myThread);
            myThread.start();
        }

        for (Thread tempThread : arrayListOfThreads) {
            tempThread.join();
        }

        matrix.printMatrix();
    }
}