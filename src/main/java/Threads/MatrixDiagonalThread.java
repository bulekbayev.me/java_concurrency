package Threads;

import Matrix.SquareMatrix;
import java.util.concurrent.locks.ReentrantLock;

public class MatrixDiagonalThread extends Thread {

    private int threadUniqueInteger;
    private SquareMatrix matrix;
    private ReentrantLock matrixElementLock;

    public MatrixDiagonalThread(SquareMatrix matrix, int threadInteger, ReentrantLock matrixElementLock) {
        this.matrix = matrix;
        this.threadUniqueInteger = threadInteger;
        this.matrixElementLock = matrixElementLock;
    }

    @Override
    public void run() {
        int indexOfTheElementInMainDiagonal = 0;
        while (indexOfTheElementInMainDiagonal < matrix.getMatrixSize()) {
            matrix.changeElementOfMatrixMainDiagonal(matrixElementLock, threadUniqueInteger, indexOfTheElementInMainDiagonal);
            indexOfTheElementInMainDiagonal++;
        }
    }
}

